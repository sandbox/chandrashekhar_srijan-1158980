Purpose
The high_water_mark_query_parameter module provides a simple interface to add/edit the parameter added to a complex query for boosting the query performance.

This way a large number of row is simply skipped while fetching the records, results in performance gain by 80%-95% with any kind of complex.
Best Usage

Custom Modules: Complex queries normally process lots of rows to return very less records.

Often sites show latest few records on their home page. Such queries normally process thousands of records to return just 5-20 records. Such complex queries are best suited to utilize this feature.
How to mark/obtain High water mark parameter

   1. Original Query : pass "%s" in where caluse
   2. Query Parameter: pass "and 1=1" initially
   3. Query Fields: set exclusively the fields which are controlling the query. Rows with these fields value will be stored.
   4. Save the parameter
   5. See the listing
   6. Find the minimum value of controlling field from the result set shown. For now it is "nid"
   7. Find the minumum value of nid
   8. Now you have got minimum value of nid as 504 or some value
   9. Click on the Modify Link
  10. Query Parameter: set "and n.nid> 503"
  11. Save the parameter

This way nid, uid, tid or based on the type/nature of the query, the "Query Parameter" can be defined for the high water marking.
This is important step in defining the High water mark query parameter.

How to define a parameter
Following are the integral part of the High water mark parameters definition:
   1. Name: have a unique name for query parameter
   2. Query Parameter: place a query for the parameter
   3. Original Query: have original query
   4. Status: set the status for the inclusion or exclusion of parameter from the usage

When the parameter is being created, the "Query Parameter" gets inserted into the body of the "Original Query". If row(s) returns, the parameter will be created with the result set, otherwise error will be flagged.

This way interface allows to update the "Query Parameter" and run the test against the "Original Query". If the difference is too high, change the value of the "Query Parameter".

How to use a defined parameter
   1. Call the function high_water_mark_query_parameter_get in the custom query
   2. Pass the "Name" of the High Water Mark Query Parameter

If the "parameter" exists and its status is enabled, the additional query part added with "Query Parameter" is returned by the function.
Example

$query="select * from node n where n.status=1 %s order by n.created desc limit 3";
$result = db_query($query, high_water_mark_query_parameter_get('fetch_latest_3'));

The "Query Parameter" is controlled from the interface and update to this could be made as the high water mark goes up. 


Example::
There is function in which these lines of code is present. This function called many times with different tid passed each time. 
tid = 30, 175, 174, 147, 146, 33, 35, 34, 32, 36, 38, 583, 42, 40, 41, 44, 45, 3765, 46, 61, 48
$num_nodes = 3; //for all others top menu
	$sql = "SELECT * FROM {node} n
	LEFT JOIN {content_type_article} cta ON cta.nid = n.nid 
	LEFT JOIN {node_revisions} r ON n.nid = r.nid 
	LEFT JOIN {content_field_image} cfi ON n.nid = cfi.nid 
	LEFT JOIN {files} f ON cfi.field_image_fid = f.fid 
	LEFT JOIN {term_node} tn ON n.nid = tn.nid 
	LEFT JOIN {term_data} td ON td.tid = tn.tid 
	WHERE n.status =1 AND n.type = 'article' AND n.nid IN
	   (SELECT n.nid FROM {node} n 
	   LEFT JOIN {term_node} tn ON n.nid = tn.nid 
	   WHERE tn.tid = ". $tid .") GROUP BY cfi.nid
	   ORDER BY cta.field_web_ex_date_value DESC, n.created DESC LIMIT $num_nodes";
	$result = db_query(db_rewrite_sql($sql);

With above query and passing each tid specified, it processes over 20,000 records to return back just 3 records.


Now Adding high water mark concept in this query::
	$sql = "SELECT * FROM {node} n
	LEFT JOIN {content_type_article} cta ON cta.nid = n.nid 
	LEFT JOIN {node_revisions} r ON n.nid = r.nid 
	LEFT JOIN {content_field_image} cfi ON n.nid = cfi.nid 
	LEFT JOIN {files} f ON cfi.field_image_fid = f.fid 
	LEFT JOIN {term_node} tn ON n.nid = tn.nid 
	LEFT JOIN {term_data} td ON td.tid = tn.tid 
	WHERE n.status =1 AND n.type = 'article' AND n.nid IN
	   (SELECT n.nid FROM {node} n 
	   LEFT JOIN {term_node} tn ON n.nid = tn.nid 
	   WHERE tn.tid = ". $tid .") %s GROUP BY cfi.nid
	   ORDER BY cta.field_web_ex_date_value DESC, n.created DESC LIMIT $num_nodes";
	$result = db_query(db_rewrite_sql($sql,high_water_mark_query_parameter_get('getoverlay_' . $subterm->tid)));

Now the query is altered. Two major changes are
1. passed %s in where clause
2. added high_water_mark_query_parameter_get('getoverlay_' . $subterm->tid) while executing the query.

Now don't worry. Since there is no variable defined, nothing will happen to code. The high_water_mark_query_parameter_get returns blank when the variable being called doesn't exist or it is disabled.

Any way, we have done our work with code to have high water mark setting in the code.
Now visit these links. It will help how to create and set the high water mark parameter for this dynamic kind of query.

Go to this path: admin/settings/hwmq/add and pass these values
Name:getoverlay_30
Original Query: SELECT * FROM {node} n
	LEFT JOIN {content_type_article} cta ON cta.nid = n.nid 
	LEFT JOIN {node_revisions} r ON n.nid = r.nid 
	LEFT JOIN {content_field_image} cfi ON n.nid = cfi.nid 
	LEFT JOIN {files} f ON cfi.field_image_fid = f.fid 
	LEFT JOIN {term_node} tn ON n.nid = tn.nid 
	LEFT JOIN {term_data} td ON td.tid = tn.tid 
	WHERE n.status =1 AND n.type = 'article' AND n.nid IN
	   (SELECT n.nid FROM {node} n 
	   LEFT JOIN {term_node} tn ON n.nid = tn.nid 
	   WHERE tn.tid = 30) %s GROUP BY cfi.nid
	   ORDER BY cta.field_web_ex_date_value DESC, n.created DESC LIMIT 3
Query Parameter:and 1=1
Query Fields:nid,title
comments:
Status:Enabled or disabled 
Note : we  have passed tid=30, since checking will be done against this tid value.
Now click on Save to create the High water mark parameter.
Now with confirmation message, will get the path at which parameter report can be seen, if there is no problem in query and it is fetching some record. 
So either click on the link provided or click on the listing to find the created record. You will get report with the parameter and its other details.
 
nid	title
----------------------------
7719	the princess of arts
7695	a man of leisure
7655	even gods must die

Now as we can see it has three record with lowest nid value of 7655. So this value can be now added in the Query parameter.
So click on the Modify.
change the Query Parameter with this value::
Query Parameter:and n.nid > 7652
 and save this record.


Now onwards whenever the query is executed for tid=30 , it will skip all the nid below 7653 and thus now examing 700 rows to return 3 rows.

Similary, this High water mark parameter creation can be repeatedfor each tid. 
Note: When we laod a parameter for modification and change the parameter name, it creates new parameter. So these steps could be easy for similar kind of queries.

So now with one setup created, different HWMP could be created with very ease for each tid. The name will for such each tid will be like
getoverlay_30
getoverlay_175
getoverlay_174
getoverlay_147
getoverlay_146
getoverlay_33
getoverlay_35
getoverlay_34
getoverlay_32
getoverlay_36
getoverlay_38
getoverlay_583
getoverlay_42
getoverlay_40
getoverlay_41
getoverlay_44
getoverlay_45
getoverlay_3765
getoverlay_46
getoverlay_61
getoverlay_48

SO inside Original Query, pass tid=<actual tid value > for which HWMP is being created, You will find different result set for each tid, and based on the result set change the Query parameter.
e.g. tid=38
Query parameter: and n.nid > 8771

The Query parameter sits inside Original Query with %s passed and the output is captured with Query fields specified to determine the correct value of nid (in this case) to set the criteria. This Query Parameter is going to return inside the actual line of code at which this HWMP is called and thus boosts the performance skipping certain node in the processing.
